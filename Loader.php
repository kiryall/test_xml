<?php
/**
 * XML get yandex.feeds
 * PHP version 5
 *
 * @category MyCategory
 * @package  MyPackage
 * @author   Kirill <kiryall@yandex.ru>
 * @license  https://kirill.ru PHP License
 * @link     https://kirill.ru
 */

/**
 * XML get yandex.feeds Class Loader
 * PHP version 5
 *
 * @category MyCategory
 * @package  MyPackage
 * @author   Kirill <kiryall@yandex.ru>
 * @license  https://kirill.ru PHP License
 * @link     https://kirill.ru
 */
class Loader
{
    public static $content;

    /**
     *  Загружает информацию с указанного URL
     *
     * @param string $url входяящий URL
     *
     * @return string $content
     */
    public function getContent($url)
    {
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120);
        curl_setopt($ch, CURLOPT_TIMEOUT, 120);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        self::$content = curl_exec($ch);

        curl_close($ch);

    }
}
