<?php
/**
 * XML get yandex.feeds
 * PHP version 5
 *
 * @category MyCategory
 * @package  MyPackage
 * @author   Kirill <kiryall@yandex.ru>
 * @license  https://kirill.ru PHP License
 * @link     https://kirill.ru
 */

/**
 * XML get yandex.feeds Class XML
 * PHP version 5
 *
 * @category MyCategory
 * @package  MyPackage
 * @author   Kirill <kiryall@yandex.ru>
 * @license  https://kirill.ru PHP License
 * @link     https://kirill.ru
 */
class Xml
{
    /**
     *  Удаляет ненужную информацию из XML и сохраняет новый документ
     *
     * @param string $name входяящий URL
     *
     * @return void
     */
    public function delete($name)
    {
        $dom = new DOMDocument;
        $dom->loadXML($name);
        $feed = $dom->documentElement;
        $offer = $feed->getElementsByTagName('building-name');
        $count = $feed->getElementsByTagName('building-name')->length;

        for ($i = 0; $i <= $count; $i++) {

            if ($offer->item(0)->nodeValue !== 'ЖК "Зелёные аллеи"') {

                $node = $offer->item(0);
                //var_dump($node);
                $parentNode = $node->parentNode;
                //var_dump($parentNode);
                $parentNode->parentNode->removeChild($parentNode);

            }

        }

        $dom->formatOutput = true;

        $dom->save('C:\OpenServer\domains\xml2.0.com\realty-feed.xml');
    }
}
