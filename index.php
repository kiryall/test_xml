<?php
/**
 * XML get yandex.feeds
 * PHP version 5
 *
 * @category MyCategory
 * @package  MyPackage
 * @author   Kirill <kiryall@yandex.ru>
 * @license  https://kirill.ru PHP License
 * @link     https://kirill.ru
 */

spl_autoload_register(
    function ($class) {
        $file = 'C:\OpenServer\domains\xml2.0.com\\' .
            str_replace('\\', '/', $class) . '.php';
        if (is_file($file)) {
            include_once $file;
        }
    }
);

Loader::getContent('http://matrix-web.pro/ApartmentFeeds/yandex.xml');

Xml::delete(Loader::$content);